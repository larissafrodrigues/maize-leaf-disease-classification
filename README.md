
# Maize leaf disease classification using Convolutional Neural Networks and hyperparameter optimization

## Authors
Erik Lucas da Rocha <erik.rocha@ufv.br>

Larissa Ferreira Rodrigues <larissa.f.rodrigues@ufv.br>

João Fernando Mari <joaof.mari@ufv.br>

## Citation
Paper available [here](https://sol.sbc.org.br/index.php/wvc/article/view/13489/13337)

```
@inproceedings{Rocha2020,
 author = {Erik Lucas da Rocha and Larissa Rodrigues and João Fernando Mari},
 title = {Maize leaf disease classification using convolutional neural networks and hyperparameter optimization},
 booktitle = {Anais do XVI Workshop de Visão Computacional},
 location = {Evento Online},
 year = {2020},
 keywords = {},
 pages = {104--110},
 publisher = {SBC},
 address = {Porto Alegre, RS, Brasil},
 url = {https://sol.sbc.org.br/index.php/wvc/article/view/13489}
}

```

## Highlights
- To compare different CNN models to classify maize leaf diseases.

- CNN hyperparameters optimized through Bayesian Optimization

- Performance comparison is done with state-of-the-art models.

- The best result obtained an accuracy of 97%.


## Image dataset
| Class       | Sample |
|-------------|--------|
| Leaf Spot   |   513  |
| Common Rust |  1192  |
| Late Blight |   985  |
| Healthy     |  1162  |
| Total       |  3852  |

Images were taken from [PlantVillage](https://github.com/spMohanty/PlantVillage-Dataset) Dataset

## Technical Details
- Python 3.6
- PyTorch 1.4
