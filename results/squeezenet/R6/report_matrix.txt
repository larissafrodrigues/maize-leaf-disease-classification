              precision    recall  f1-score   support

           0       0.84      0.89      0.87        85
           1       1.00      0.99      1.00       199
           2       0.94      0.91      0.92       164
           3       0.99      1.00      1.00       194

    accuracy                           0.96       642
   macro avg       0.94      0.95      0.95       642
weighted avg       0.96      0.96      0.96       642


Training complete in 5m 9s
Best valid accuracy: 0.961059
Acurácia: 0.9610591900311527
