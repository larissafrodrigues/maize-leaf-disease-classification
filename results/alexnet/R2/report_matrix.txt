              precision    recall  f1-score   support

           0       0.91      0.80      0.85        86
           1       1.00      0.99      1.00       199
           2       0.90      0.96      0.93       164
           3       1.00      1.00      1.00       194

    accuracy                           0.96       643
   macro avg       0.95      0.94      0.94       643
weighted avg       0.96      0.96      0.96       643

Training complete in 2m 23s
Best valid accuracy: 0.961120
Acurácia: 0.9611197511664075